import logo from './logo.svg';
import './App.css';
import { GoogleAuthProvider, onAuthStateChanged, signInWithPopup, signOut } from 'firebase/auth';
import auth from './firebase';
import { useEffect, useState } from 'react';

function App() {
  const [user, setUser] = useState(null);

  //khởi tạo GoogleAuthProvider
  const provider = new GoogleAuthProvider();

  const signInGoogle = () => {
    signInWithPopup(auth, provider)
    .then((result) => {
      console.log(result);
      setUser(result.user);
    })
    .catch((error) => {
      console.log(error);
      setUser(null);
    })
  }

  const signOutGoogle = () => {
    signOut(auth)
    .then(() => {
      setUser(null);
    })
    .catch((error) => {
      console.log(error);
      setUser(null);
    })
  }

  //xử lý tình huống vào trang thì check login
  useEffect(() => {
    onAuthStateChanged(auth, (loggedUser) => {
      console.log("check login status ...");
      console.log(loggedUser);
      setUser(loggedUser);
    })
  },[])

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        {
          user ?
          <>
            <img src={user.photoURL} style={{width:"60px", borderRadius:"50%", margin:"10px"}} />
            Hello {user.displayName}
            <button onClick={signOutGoogle}>Sign out</button>
          </>
          :
          <>
            <p>Please sign in</p>
            <button onClick={signInGoogle}>Sign in with Google</button>          
          </>
        }
      </header>
    </div>
  );
}

export default App;
