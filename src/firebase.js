// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyD3ehaUBJEsNkU7Skuw0IMDQlC4kWOjw3M",
  authDomain: "devcamp-r2230.firebaseapp.com",
  projectId: "devcamp-r2230",
  storageBucket: "devcamp-r2230.appspot.com",
  messagingSenderId: "570927127569",
  appId: "1:570927127569:web:ade866b4815648f896e5b0"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

export default auth;